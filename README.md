The DLT implementation include two parts:

* Multinode-deployments
* Simulation

# Feature

The environment allows people to test their connection and deployment in a Blockchain network. The feature includes:

* Create a private Ethereum Blockchain
* Deployment of Smart Contracts and applications
* Ethereu json-rpc support
* Fast-foward and mining time on demand and configuaration
* Impersonate any account
* Listen for JSON-RPC 2.0 requests and HTTP/Websockets
* Programmatic use in Nodejs
* Verify/Signing/Exchanging transactions
* DLT-based Assisted Federated Learning
* Shapley Value implementation for evaluate valuation of data

# Environments

**Simulation**

For testing your integration, we recommend you try with a emulate Blockchain running locally in directory *sim-dlt-net/*  which provides a simulate blockchain platfoms without contraints. You can test the integration of your part to DLT by publishing your data to DLT via smart contracts.

**Requirements**:

* Docker
* Docker-compose
* NodeJs
* Python3

**Step-by-step**in this sim-dlt-net directory, we have three folders, ganache, truffle, client-app.

* Ganache: a simulation Ethereum platform running locally and open in port 8545.
* Truffle: A tool help to deploy smart contracts to DLT
* Client app: DLT clients to communicate with DLT via Smart Contracts.

**Get Started **Start Blockchain network

```
cd sim-dlt-net

docker-compose up --build
```

if you see the DLT network start with the list of 10 accounts, public/private keys with their balances, it's done. I have configured 10 ETH accounts with fixed private keys and 1000000 ETH for testing, it looks like that:

```
Available Accounts
==================
(0) 0x3aEd6406bb12Ac5c9D6FeedAEEE809cf5bF65820 (100000000000000000 ETH)
(1) 0x5FBC4991cbc39ec6C41E0aFA2f234DB73164F099 (100000000000000000 ETH)
(2) 0x8DD67FB396a8D6587ad4cA888Da31EdBbA213B19 (100000000000000000 ETH)
(3) 0xcd6e8009e6A53b505cd5Ddc02Fd04C6509b88E6F (100000000000000000 ETH)
(4) 0xaAa0B1C3C41e896108e890164c0F8f2D46eEac45 (100000000000000000 ETH)
(5) 0xc4C51B988B208784fe8a9035D6B54548E27043Ac (100000000000000000 ETH)
(6) 0x6388402E5e21891dF3Ab6ac79823847aCb83D83d (100000000000000000 ETH)
(7) 0xc1eaa48D34ECa11606C7A8A36CC2482c6e6fE30f (100000000000000000 ETH)
(8) 0xebE55F85dD8c8E0019e3B081b5c4691365477A62 (100000000000000000 ETH)
(9) 0x81b4d67a65C9C8D0969D71E8D2e9f3e8CcE5935d (100000000000000000 ETH)
(10) 0x122FfC92fF8d3fC7e7b448f6401E4D2716f06f72 (100000000000000000 ETH)
(11) 0x0e87A55CA2ACEd6b517F80a5D195fF4db4982cCD (100000000000000000 ETH)
(12) 0xBCB026c6a940B965D3965EA79f5c0C2150481908 (100000000000000000 ETH)
(13) 0x80358309c8a0758B992A26f2ae104255cC31A98C (100000000000000000 ETH)
(14) 0x9Cd8953a956F49D720d4A9609184965679a95a7E (100000000000000000 ETH)
(15) 0xF38BD4460C2934f305127492B0bF6F15236bd91f (100000000000000000 ETH)
(16) 0x2CB3A4dAFAC5803F1D8010432d8571BA2fb82009 (100000000000000000 ETH)
(17) 0x997F2Dcf4D4e40AdB7c9C2303555ff1fa639BDd0 (100000000000000000 ETH)
(18) 0x3f70043f3c2d374fD449BA87C58FFE0E7a60442b (100000000000000000 ETH)
(19) 0x67699d215c580C6382d96d14757a24370BD3d603 (100000000000000000 ETH)

Private Keys
==================
(0) 0x03093de5fad0147ce254da8d65d51ccaa84f9e36a83ec9c24f8fddbc47bae247
(1) 0x6a94e547f4fd8b260b34e5baa7a94ec9dc95b78b756c894b002cb7006f909d39
(2) 0xdae5686d0c28422753d5b56482b5c93cf58ebefeefefe08ffd09dfe838eac0fc
(3) 0xf458d5b1543c17453d440d68593b982533f2c123d3f5d9e97e67126a0ae95495
(4) 0x7fde54fb18d3c612b2cee7da4d7f7711d9635bc06e359b144daebd456b153317
(5) 0x4de15a5b4be86c4f1ad6d16b37fbeec671a2f201be70b63b279b143c54c03f40
(6) 0x83ba48d5412048c6dd909f4aefb4c32cc4efcbf84e88a59f07c42c7ef9139a11
(7) 0xf4c7be386300df9ccf37a3e6f231d8d59bf8acc1afd0c8c75cd3279fa3655e1e
(8) 0x8f85808c46dbeac1043ef1a23bc0def37e1047909dbfb7bc5802a0d279482ea6
(9) 0x57ec75b8eb9788bc4e9a77d286715f6515c76863b98adc720c86e985804082f1
(10) 0x1475f53a1a09b8fd311080979e58e35f480bda22d9092a2b446623a799c6c83f
(11) 0xba9c1a0ef5266c46ab501d0e36ea7d7d169bf04f9f0e2231a0229d5fc7524395
(12) 0x76da2b522d5e4a7ab515dd9442fa3c426439f94a7e3e7147cc65efba76c3f676
(13) 0x57320e7494edd88cb20151a6646119cd47987ddc3a4d9d1e31562db3e42169e7
(14) 0x1bdb5a116c593f1e20ee9a6208b81fdbe0f34a138d8b4793863abfd87e8ea733
(15) 0xb0988b6f257a09c242d88821e73abf490389b6dd664a49bae72c8a4813c2b593
(16) 0x1d003e77ed9a5c7e58a78bdb204c82ab33c9a06f59e871b3ce9cb88fe099542c
(17) 0x784b8415de2213fe1185da3f9a2096563eb551528b697dbfcbcfda4df88c2cc5
(18) 0x6bbdd8a58a86b91e78bc6880ee44999b5ce40753b68e242cf77497987bf4b0c8
(19) 0xa395da11ebf2389dc3efd22c8c46835c24054b13d144f7426a3718f2c680d89b

HD Wallet
==================
Mnemonic:      area heavy wrong air safe dynamic life milk extra feel priority oven
Base HD Path:  m/44'/60'/0'/0/{account_index}


```

and the smart contract is deployed. In this example, a simple smart contract to record strings to DLT is implement. It looks like:

```
ruffle_suite    |    > contract address:    0xDca4261fB51136E65700Ef63fb7f18943A658094
truffle_suite    |    > block number:        7
truffle_suite    |    > block timestamp:     1633947517
truffle_suite    |    > account:             0x2E6d29B17a526334843141F8BEf0f0424760199e
truffle_suite    |    > balance:             99999999.975605839999999999
truffle_suite    |    > gas used:            361904 (0x585b0)
truffle_suite    |    > gas price:           20 gwei
truffle_suite    |    > value sent:          0 ETH
truffle_suite    |    > total cost:          0.00723808 ETH

```

Application

We basically have done with creat a Blockchain and deploy smart contracts. Now we go to the client part, and how to record the data to Blockchain.

```
cd client-app
python app.py
```

The outshould be:

```

=============================================================================
Successfully connected to Testnet
Setting gas price strategy. Please wait...
Estimated gas price 0 gwei
=============================================================================
Mon Oct 11 13:03:21 2021        Temperature:20.042076424662977C Humidity:29.457927022410857%
-----------------------------------------------------------------------------
Transaction:
{'value': 0, 'gas': 28876, 'gasPrice': 20000000000, 'chainId': 1337, 'nonce': 78, 'to': '0xDca4261fB51136E65700Ef63fb7f18943A658094', 'data': '0xa40b6d66000000000000000000000000000000000000000000000000000000000000006000000000000000000000000000000000000000000000000000000000000000a000000000000000000000000000000000000000000000000000000000000000e000000000000000000000000000000000000000000000000000000000000000184d6f6e204f63742031312031333a30333a323120323032310000000000000000000000000000000000000000000000000000000000000000000000000000001232302e3034323037363432343636323937370000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001232392e3435373932373032323431303835370000000000000000000000000000'}
-----------------------------------------------------------------------------
Successfully sent to Ethereum
gas used: 28876
contract address: 0xDca4261fB51136E65700Ef63fb7f18943A658094
tx: 0x5b688ac8af697d1caf00657748838858099084902c966cacbbb860bd29f85e21
block: 0xed68bb799e877ab6c5b00d94cb0d0cb21b15e3c28308e06b435ad8728398b03a
```

In app.py, we define the Blockchain network : ` Web3(Web3.HTTPProvider("http://127.0.0.1:8545"))`, and we send the sample data of  timestamp, temparature, humididty data generated randomly via:

```
	def send_tx():
		try:
			timestamp = time.asctime()
			temperature = random.uniform(11, 30)
			humidity = random.uniform(11, 30)
			print("{}Temperature:{}CHumidity:{}%".format(timestamp, temperature, humidity))
			print("-----------------------------------------------------------------")

		except RuntimeError as error:
			print("Failed to get temperature and humidity. Please try again...")
			print(error)
			print("=============================================================")
			return

		timestamp_encode = encode(timestamp)
		temperature_encode = encode(temperature)
		humidity_encode = encode(humidity)
		nonce = w3.eth.getTransactionCount(account_address)
		function = contract.functions.add_records(timestamp_encode, temperature_encode, humidity_encode)
		tx = function.buildTransaction({"nonce": nonce})
		print("Transaction:")
		print(tx)
		print("-----------------------------------------------------------------------------")
```

To sign, send and get receipt:

```
			#sign  transaction
			signed_tx = w3.eth.account.signTransaction(tx, private_key)

			#Send tranaction
			tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)

			#get receive
			receipt = w3.eth.waitForTransactionReceipt(tx_hash)

			print("Successfully sent to Ethereum")
			print("gas used:", receipt.cumulativeGasUsed)
			print("contract address:", contract_address)
			print("tx:", Web3.toHex(receipt.transactionHash))
			print("block:", Web3.toHex(receipt.blockHash))
```

To sign, send and get receipt:

```
			#sign  transaction
			signed_tx = w3.eth.account.signTransaction(tx, private_key)

			#Send tranaction
			tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)

			#get receive
			receipt = w3.eth.waitForTransactionReceipt(tx_hash)

			print("Successfully sent to Ethereum")
			print("gas used:", receipt.cumulativeGasUsed)
			print("contract address:", contract_address)
			print("tx:", Web3.toHex(receipt.transactionHash))
			print("block:", Web3.toHex(receipt.blockHash))
```

You can replace the sample input by your data. The output look likes:

```
Mon Oct 11 13:04:04 2021        Temperature:28.43027512392157C  Humidity:11.336607394049805%
```

and the hash of transactions:

```
Successfully sent to Ethereum
gas used: 28876
contract address: 0xDca4261fB51136E65700Ef63fb7f18943A658094
tx: 0xa99d0ee0086bf5dfea3bad8cda87cf863d9d60d97e9130c88ebdc6fae593016f
block: 0xa71e28f2f4825ad67552cfb141edbc7031f728b41bb64125576c3f3209d6641c
```

Based on the hash of transaction, we can reveal the detail of data published to Blockchain. Another example of send transactions to blockchain can be find in *send-transction.py* or *send-transaction.js*

The detail and all needed API can be found [here](https://web3js.readthedocs.io/en/v1.5.2/web3-eth.html#gettransactionreceipt).

In case you want to deep dive into fields of a transactions to get info, the JSON-RPC detail [here](https://eth.wiki/json-rpc/API).

# Multi-node Deployment
