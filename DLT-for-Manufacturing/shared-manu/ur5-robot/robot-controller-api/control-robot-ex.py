#!/usr/bin/env python

import sys
sys.path.append('/home/user/intelliot/distributed-ledger-technology/DLT-for-Manufacturing/shared-manu/ur5-robot/')
import logging

import rtde.rtde as rtde
import rtde.rtde_config as rtde_config


logging.basicConfig(level=logging.INFO)

ROBOT_HOST = '10.0.2.4'
ROBOT_PORT = 30004
config_filename = 'control_loop_configuration.xml'

keep_running = True

logging.getLogger().setLevel(logging.INFO)

conf = rtde_config.ConfigFile('/home/user/intelliot/distributed-ledger-technology/DLT-for-Manufacturing/shared-manu/ur5-robot/robot-controller-api/control_loop_configuration.xml')
state_names, state_types = conf.get_recipe('state')
setp_names, setp_types = conf.get_recipe('setp')
watchdog_names, watchdog_types = conf.get_recipe('watchdog')

con = rtde.RTDE(ROBOT_HOST, ROBOT_PORT)
con.connect()

# get controller version
con.get_controller_version()

# setup recipes
con.send_output_setup(state_names, state_types)
setp = con.send_input_setup(setp_names, setp_types)
watchdog = con.send_input_setup(watchdog_names, watchdog_types)

# Setpoints to move the robot to
setp1 = [-0.12, -0.43, 0.14, 0, 3.11, 0.04]
setp2 = [-0.22, 0.51, 0.21, 0, 1.11, 0.08]
setp3 = [-0.22, 0.51, 0.21, 0, 0.11, 0.18]
setp4 = [-0.22, 0.51, 0.11, 0, 0.31, 0.18]
setp5 = [-0.22, 0.51, 0.11, 0, 0.11, 0.18]

setp.input_double_register_0 = 0
setp.input_double_register_1 = 0
setp.input_double_register_2 = 0
setp.input_double_register_3 = 0
setp.input_double_register_4 = 0
setp.input_double_register_5 = 0
  
# The function "rtde_set_watchdog" in the "rtde_control_loop.urp" creates a 1 Hz watchdog
watchdog.input_int_register_0 = 0

def setp_to_list(setp):
    list = []
    for i in range(0,6):
        list.append(setp.__dict__["input_double_register_%i" % i])
    return list

def list_to_setp(setp, list):
    for i in range (0,6):
        setp.__dict__["input_double_register_%i" % i] = list[i]
    return setp

#start data synchronization
if not con.send_start():
    sys.exit()

# control loop
while keep_running:
    # receive the current state
    state = con.receive()
    
    if state is None:
        break;
    
    # do something...
    if state.output_int_register_0 != 0:
        new_setp = setp1 if setp_to_list(setp) == setp2 else setp2 if setp_to_list(setp) == setp3 else setp3 if setp_to_list(setp) == setp4 else setp4 if setp_to_list(setp) == setp5 else setp5
        list_to_setp(setp, new_setp)
        # send new setpoint        
        con.send(setp)

    # kick watchdog
    con.send(watchdog)

con.send_pause()

con.disconnect()
