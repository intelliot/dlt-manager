import json
import time
import time
import random
import json
import hashlib
import web3
from web3 import Web3
from web3.gas_strategies.time_based import (fast_gas_price_strategy, 
					    medium_gas_price_strategy,
					    slow_gas_price_strategy,
					    glacial_gas_price_strategy)


lightlist = ('green', 'red', 'blue')

def main():
	print("=============================================================================")

	w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545"))
	if w3.isConnected():
		print("Successfully connected to Testnet")
	else:
		print("Failed to connect to Testnet, please check your network")
		return

	led = True

	print("Setting gas price strategy. Please wait...")
	w3.eth.setGasPriceStrategy(fast_gas_price_strategy)
	gas_price = w3.eth.generateGasPrice()
	print("Estimated gas price {} gwei".format(Web3.fromWei(gas_price, "gwei")))

	contract_address = "0xD7Cee1f279eD244d2d7f8eDbbF5BdA4C506A1Ae2"
	
	abi = json.loads('[ { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "_to", "type": "address" }, { "indexed": false, "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "led", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": false, "internalType": "bytes", "name": "time", "type": "bytes" }, { "indexed": false, "internalType": "bytes", "name": "temp", "type": "bytes" }, { "indexed": false, "internalType": "bytes", "name": "hum", "type": "bytes" } ], "name": "records", "type": "event" }, { "constant": false, "inputs": [], "name": "destory", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "bytes", "name": "time", "type": "bytes" }, { "internalType": "bytes", "name": "temp", "type": "bytes" }, { "internalType": "bytes", "name": "hum", "type": "bytes" } ], "name": "add_records", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "address", "name": "_to", "type": "address" }, { "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "control_led", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" } ]')

	account_address = Web3.toChecksumAddress(0x6505F8516d4FdbFCdA5E51816E128a0B89FF9C15)
	
	with open('/home/user/intelliot/dlt-benchmark/eth-testnet/1-dltNetwork/miner1/keystore/UTC--2021-02-24T23-33-27.482962000Z--6505f8516d4fdbfcda5e51816e128a0b89ff9c15') as keyfile: 
		encrypted_key = keyfile.read()
		private_key = w3.eth.account.decrypt(encrypted_key, '123')
	#private_key = "0xf90c16e1628661bc15769b76c8266343ec35db482239904b0ca1bd90c8fbca01"

	contract = w3.eth.contract(address=contract_address, abi=abi)
	event_filter = contract.events.led.createFilter(fromBlock="latest", argument_filters={"_to": account_address})
	

	print("=============================================================================")

	def encode(x):
		return str(x).encode()

	def send_tx():
		try:
			timestamp = time.asctime()
			temperature = random.uniform(11, 30)
			humidity = random.uniform(11, 30)
			print("{}	Data1:{}C	Data2:{}%".format(timestamp, temperature, humidity))
			print("-----------------------------------------------------------------------------")

		except RuntimeError as error:
			print("Failed to get temperature and humidity. Please try again...")
			print(error)
			print("=============================================================================")
			return

		timestamp_encode = encode(timestamp)
		temperature_encode = encode(temperature)
		humidity_encode = encode(humidity)
		nonce = w3.eth.getTransactionCount(account_address)
		function = contract.functions.add_records(timestamp_encode, temperature_encode, humidity_encode)
		tx = function.buildTransaction({"nonce": nonce})
		print("Transaction:")
		print(tx)
		print("-----------------------------------------------------------------------------")
		
		try:
			#sign  transaction
			signed_tx = w3.eth.account.signTransaction(tx, private_key)
			
			#Send tranaction
			tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)
			
			#get receive
			receipt = w3.eth.waitForTransactionReceipt(tx_hash)
			
			print("Successfully sent to Ethereum")
			print("gas used:", receipt.cumulativeGasUsed)
			print("contract address:", contract_address)
			print("tx:", Web3.toHex(receipt.transactionHash))
			print("block:", Web3.toHex(receipt.blockHash))
		except Exception as e:
			print(e)
		print("=============================================================================")

	while True:
	 	for event in event_filter.get_new_entries():
	 		print("from:", event.args._from)
	 		print("tx:", Web3.toHex(event.transactionHash))
	 		print("block:", Web3.toHex(event.blockHash))
	 		print("=============================================================================")
		
	 	send_tx()
	 	time.sleep(10)


if __name__ == "__main__":
    main()