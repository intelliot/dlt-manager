package com.ethereum.connection;

import java.io.IOException;
import java.math.BigInteger;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;
import org.web3j.utils.Convert.Unit;

import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthGetTransactionReceipt;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.utils.Numeric;


public class EthereumConnector {

	public static void main(String[] args) throws Exception {

		System.out.println("Connecting to Ethereum ...");
		Web3j web3 = Web3j.build(new HttpService("HTTP://127.0.0.1:8545"));//RPC SERVER
		System.out.println("Successfuly connected to Ethereum");

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		try {
			String  privetKey= "0x03093de5fad0147ce254da8d65d51ccaa84f9e36a83ec9c24f8fddbc47bae247"; // Add a private key here
			// Decrypt private key into Credential object
			Credentials credentials = Credentials.create(privetKey);
			System.out.println("Account address: " + credentials.getAddress());
			System.out.println("Balance: "
					+ Convert.fromWei(web3.ethGetBalance(credentials.getAddress(), DefaultBlockParameterName.LATEST)
							.send().getBalance().toString(), Unit.ETHER));

			// Get the latest nonce of current account
			EthGetTransactionCount ethGetTransactionCount = web3
					.ethGetTransactionCount(credentials.getAddress(), DefaultBlockParameterName.LATEST).send();
			BigInteger nonce = ethGetTransactionCount.getTransactionCount();

			// Recipient address
			String recipientAddress = "0x5FBC4991cbc39ec6C41E0aFA2f234DB73164F099";
			// Value to transfer (in wei)
			System.out.println("Enter Amount to be sent:");
			String amountToBeSent= "1";
			BigInteger value = Convert.toWei(amountToBeSent, Unit.ETHER).toBigInteger();
			String data = "Nguyen duc lam test";

			// Gas Parameter
			BigInteger gasLimit = BigInteger.valueOf(100000);
			BigInteger gasPrice = Convert.toWei("1", Unit.GWEI).toBigInteger();

			// Prepare the rawTransaction
			RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, gasPrice, gasLimit,
					recipientAddress, value,  data);

			// Sign the transaction
			byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
			String hexValue = Numeric.toHexString(signedMessage);

			System.out.println("hex value:"+ hexValue);

			// Send transaction
			EthSendTransaction ethSendTransaction = web3.ethSendRawTransaction(hexValue).send();
			String transactionHash = ethSendTransaction.getTransactionHash();

			System.out.println("transactionHash: " + transactionHash);

			// Wait for transaction to be mined
			Optional<TransactionReceipt> transactionReceipt = null;
			do {
				System.out.println("checking if transaction " + transactionHash + " is mined....");
				EthGetTransactionReceipt ethGetTransactionReceiptResp = web3.ethGetTransactionReceipt(transactionHash).send();
				transactionReceipt = ethGetTransactionReceiptResp.getTransactionReceipt();
				Thread.sleep(3000); // Wait for 3 sec
			} while (!transactionReceipt.isPresent());

			System.out.println("Transaction " + transactionHash + " was mined in block # "
					+ transactionReceipt.get().getBlockNumber());
			System.out.println("Balance: "
					+ Convert.fromWei(web3.ethGetBalance(credentials.getAddress(), DefaultBlockParameterName.LATEST)
							.send().getBalance().toString(), Unit.ETHER));

		} catch (IOException ex) {
			throw new RuntimeException("Error whilst sending json-rpc requests", ex);
		}
	}
}