#!/usr/bin/env python

import argparse
import logging
import sys
sys.path.append('/home/user/intelliot/distributed-ledger-technology/DLT-for-Manufacturing/shared-manu/ur5-robot/')
import rtde.rtde as rtde
import rtde.rtde_config as rtde_config
import rtde.csv_writer as csv_writer
import rtde.csv_binary_writer as csv_binary_writer

import json
import time
import random
import json
import web3
from web3 import Web3
from web3.gas_strategies.time_based import (fast_gas_price_strategy, 
					    medium_gas_price_strategy,
					    slow_gas_price_strategy,
					    glacial_gas_price_strategy)


#parameters
parser = argparse.ArgumentParser()
parser.add_argument('--host', default='10.0.2.4', help='name of host to connect to (robot controller)')
parser.add_argument('--port', type=int, default=30004, help='port number (30004)')
parser.add_argument('--samples', type=int, default=0, help='number of samples to record')
parser.add_argument('--frequency', type=int, default=125, help='the sampling frequency in Herz')
parser.add_argument('--config', default='record_configuration.xml', help='data configuration file to use (record_configuration.xml)')
parser.add_argument('--output', default='robot_data.csv', help='data output file to write to (robot_data.csv)')
parser.add_argument("--verbose", help="increase output verbosity", action="store_true")
parser.add_argument("--buffered", help="Use buffered receive which doesn't skip data", action="store_true")
parser.add_argument("--binary", help="save the data in binary format", action="store_true")
args = parser.parse_args()

logging.basicConfig(level=logging.INFO)

if args.verbose:
    logging.basicConfig(level=logging.INFO)

conf = rtde_config.ConfigFile(args.config)
output_names, output_types = conf.get_recipe('out')


#################### Connect with RTDE 

con = rtde.RTDE(args.host, args.port)
con.connect()


############## Connect to the DLT 
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545"))
if w3.isConnected():
	print("Successfully connected to Testnet")
else:
	print("Failed to connect to Testnet, please check your network")

########## Setting for DLTs network 
print("Setting gas price strategy. Please wait...")
w3.eth.setGasPriceStrategy(fast_gas_price_strategy)
gas_price = w3.eth.generateGasPrice()
print("Estimated gas price {} gwei".format(Web3.fromWei(gas_price, "gwei")))

contract_address = "0xDca4261fB51136E65700Ef63fb7f18943A658094"
	
abi = json.loads('[ { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "_to", "type": "address" }, { "indexed": false, "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "led", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": false, "internalType": "bytes", "name": "time", "type": "bytes" }, { "indexed": false, "internalType": "bytes", "name": "temp", "type": "bytes" }, { "indexed": false, "internalType": "bytes", "name": "hum", "type": "bytes" } ], "name": "records", "type": "event" }, { "constant": false, "inputs": [], "name": "destory", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "bytes", "name": "time", "type": "bytes" }, { "internalType": "bytes", "name": "temp", "type": "bytes" }, { "internalType": "bytes", "name": "hum", "type": "bytes" } ], "name": "add_records", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "address", "name": "_to", "type": "address" }, { "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "control_led", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" } ]')

account_address = Web3.toChecksumAddress(0x2E6d29B17a526334843141F8BEf0f0424760199e)
private_key = "0xf90c16e1628661bc15769b76c8266343ec35db482239904b0ca1bd90c8fbca01"

contract = w3.eth.contract(address=contract_address, abi=abi)
event_filter = contract.events.led.createFilter(fromBlock="latest", argument_filters={"_to": account_address})

print("=============================================================================")

#########################################
# get controller version
con.get_controller_version()

# setup recipes
if not con.send_output_setup(output_names, output_types, frequency = args.frequency):
    logging.error('Unable to configure output')
    sys.exit()

#start data synchronization
if not con.send_start():
    logging.error('Unable to start synchronization')
    sys.exit()

writeModes = 'wb' if args.binary else 'w'
with open(args.output, writeModes) as csvfile:
    writer = None

    if args.binary:
        writer = csv_binary_writer.CSVBinaryWriter(csvfile, output_names, output_types)
    else:
        writer = csv_writer.CSVWriter(csvfile, output_names, output_types)

    writer.writeheader()
    
    i = 1
    keep_running = True
    while keep_running:
        
        if i%args.frequency == 0:
            if args.samples > 0:
                sys.stdout.write("\r")
                sys.stdout.write("{:.2%} done.".format(float(i)/float(args.samples))) 
                sys.stdout.flush()
            else:
                sys.stdout.write("\r")
                sys.stdout.write("{:3d} samples.".format(i)) 
                sys.stdout.flush()
        if args.samples > 0 and i >= args.samples:
            keep_running = False
        try:
            if args.buffered:
                state = con.receive_buffered(args.binary)
            else:
                state = con.receive(args.binary)
            if state is not None:
                writer.writerow(state)
                i += 1

        except KeyboardInterrupt:
            keep_running = False
        except rtde.RTDEException:
            con.disconnect()
            sys.exit()
        


sys.stdout.write("\rComplete!            \n")

	


con.send_pause()
con.disconnect()


