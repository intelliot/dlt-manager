#!/bin/bash

#geth --identity "miner1" --networkid 42 --datadir "miner1" --nodiscover --mine  --rpc --rpccorsdomain "*" --rpcaddr "192.168.43.176" --rpcport "8545" --port "30303" --unlock 0 --allow-insecure-unlock --password miner1/password.sec --ipcpath "~/Library/Ethereum/geth.ipc"

geth --identity "miner1" --networkid 42 --datadir "miner1"  --mine --nodiscover --metrics  --http --http.corsdomain "*" --http.port "8545" --port "30303" --unlock 0 --allow-insecure-unlock --password miner1/password.sec --ipcpath "/home/user/Library/Ethereum/geth.ipc"
