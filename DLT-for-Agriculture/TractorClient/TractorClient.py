import requests
import json
import time
import random
import argparse
import json
from web3 import Web3
from web3.gas_strategies.time_based import (fast_gas_price_strategy,
					    medium_gas_price_strategy,
					    slow_gas_price_strategy,
					    glacial_gas_price_strategy)


parser = argparse.ArgumentParser(description="Poll coordinates from AVL")
parser.add_argument("--avl_url",  type=str, help="Assign the URL of the AVL platform", default="http://etractorapi.germanywestcentral.cloudapp.azure.com")
parser.add_argument("--dlt_mgr_ip",  type=str, help="Assigns the IP of the DLT manager", default="0.0.0.0")
args = parser.parse_args()


url=args.avl_url
manager_ip=args.dlt_mgr_ip

#url="http://etractorapi.germanywestcentral.cloudapp.azure.com/sensors/gps"
payload={}
url=f"{url}/sensors/gps"

lightlist = ('green', 'red', 'blue')

def main():
	print("=============================================================================")

	w3 = Web3(Web3.HTTPProvider("http://"+manager_ip+":8545"))
	if w3.isConnected():
		print("Successfully connected to Testnet")
	else:
		print("Failed to connect to Testnet, please check your network")
		return


	provider_rpc = {
		"development": "http://"+manager_ip+":8545",
		"alphanet": "https://rpc.testnet.moonbeam.network",
	}
	web3 = Web3(Web3.HTTPProvider(provider_rpc["development"]))  # Change to correct network

	# Variables
	account_from = {
		"private_key": "0xf90c16e1628661bc15769b76c8266343ec35db482239904b0ca1bd90c8fbca01",
		"address": "0x2E6d29B17a526334843141F8BEf0f0424760199e",
	}
	address_to = "0x2E6d29B17a526334843141F8BEf0f0424760199e"  # Change address_to

	#
	#  -- Create and Deploy Transaction --
	#
	print(
		f'Attempting to send transaction from { account_from["address"] } to { address_to }'
	)

	# Sign Tx with PK
	tx_create = web3.eth.account.signTransaction(
		{
			"nonce": web3.eth.getTransactionCount(account_from["address"]),
			"gasPrice": 1,
			"gas": 21000,
			"to": address_to,
			"value": web3.toWei("1", "ether"),
		},
		account_from["private_key"],
	)

	# Send Tx and Wait for Receipt
	tx_hash = web3.eth.sendRawTransaction(tx_create.rawTransaction)
	receipt = web3.eth.waitForTransactionReceipt(tx_hash)

	print("Test transaction to Ethereum successful")

	print("Setting gas price strategy. Please wait...")
	w3.eth.setGasPriceStrategy(fast_gas_price_strategy)
	gas_price = w3.eth.generateGasPrice()
	print("Estimated gas price {} gwei".format(Web3.fromWei(gas_price, "gwei")))

	contract_address = "0xDca4261fB51136E65700Ef63fb7f18943A658094"


	abi = json.loads('[ { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "_to", "type": "address" }, { "indexed": false, "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "led", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": false, "internalType": "bytes", "name": "time", "type": "bytes" }], "name": "records", "type": "event" }, { "constant": false, "inputs": [], "name": "destory", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "bytes", "name": "time", "type": "bytes" }], "name": "add_records", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "address", "name": "_to", "type": "address" }, { "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "control_led", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" } ]')

	account_address = Web3.toChecksumAddress(0x2E6d29B17a526334843141F8BEf0f0424760199e)

	#with open('/Users/dltnode/workspace/intellIoT/1-dltNetwork/miner1/keystore/UTC--2021-02-24T23-33-27.482962000Z--6505f8516d4fdbfcda5e51816e128a0b89ff9c15') as keyfile:
	#	encrypted_key = keyfile.read()
	#	private_key = w3.eth.account.decrypt(encrypted_key, '123')
	private_key = "0xf90c16e1628661bc15769b76c8266343ec35db482239904b0ca1bd90c8fbca01"

	contract = w3.eth.contract(address=contract_address, abi=abi)
	event_filter = contract.events.led.createFilter(fromBlock="latest", argument_filters={"_to": account_address})


	print("=============================================================================")

	def encode(x):
		return str(x).encode()

	def send_tx():
		try:
			response = requests.request("GET", url, data=payload, verify=False)
		except RuntimeError as error:
			print("Failed to get coordinates. Please try again...")
			print(error)

			return

		timestamp_encode = encode(response.text)
		print(response.text)
		nonce = w3.eth.getTransactionCount(account_address)
		function = contract.functions.add_records(timestamp_encode)
		tx = function.buildTransaction({"nonce": nonce})
		print("Transaction:")
		print(tx)
		print("-----------------------------------------------------------------------------")

		try:
			#sign  transaction
			signed_tx = w3.eth.account.signTransaction(tx, private_key)

			#Send tranaction
			tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)

			#get receive
			receipt = w3.eth.waitForTransactionReceipt(tx_hash)

			print("Successfully sent to Ethereum")
			print("gas used:", receipt.cumulativeGasUsed)
			print("contract address:", contract_address)
			print("tx:", Web3.toHex(receipt.transactionHash))
			print("block:", Web3.toHex(receipt.blockHash))
		except Exception as e:
			print(e)
		print("=============================================================================")

	while True:
	 	for event in event_filter.get_new_entries():
	 		print("from:", event.args._from)
	 		print("tx:", Web3.toHex(event.transactionHash))
	 		print("block:", Web3.toHex(event.blockHash))
	 		print("=============================================================================")

	 	send_tx()
	 	time.sleep(20)


if __name__ == "__main__":
    main()

