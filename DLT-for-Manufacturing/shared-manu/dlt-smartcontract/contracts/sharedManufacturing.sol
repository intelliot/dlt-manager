// SPDX-License-Identifier: MIT
// Smart Contracts for shared manufacturing use case
// Actors

pragma solidity ^0.4.17;

import {MachineLib} from "./MachineLib.sol";

contract Rental {
    
    bool private stopped = false;
    address private manager;

     // The constructor. We assign the manager to be the creator of the contract.It could be enhanced to support passing maangement off.
    constructor() public  
    {
        manager = msg.sender;
    }

    //Check if sender isAdmin, this function alone could be added to multiple functions for manager only method calls
    modifier isAdmin() {
        assert(msg.sender == manager);
        _;
    }
    
    //Check if the contracts features are deactivated
    function getStopped() public view returns(bool) { return stopped; }
    
    function toggleContractActive() isAdmin public {
    // You can add an additional modifier that restricts stopping a contract to be based on another action, such as a vote of users
        stopped = !stopped;
    }

    //Number of Machines available for rent
    MachineLib.Machine[] public rentals;

    //Return Total Number of Machines
    function getMachineCount() public constant returns(uint) {
        return rentals.length;
    }

    //Renting a machine  
    function rent(uint machineId) public returns (bool) { 
       
       //Never Ever want to be false, therefore we use assert 
       assert(!stopped); 
        
       //Validate machinedId is within array
      uint totalMachines = getMachineCount();
      
    //There must be a machine to rent and ID # must be within range 
      require(machineId >= 0 && machineId < totalMachines);
    
    //Reference to the machine that will be rented 
    MachineLib.Machine storage machineToBeRented = rentals[machineId];
    
    //Machine must be available
    require(machineToBeRented.isAvailable == true);
      
      //Assign Rentee to Sender
      machineToBeRented.rentee = msg.sender;
      
      //Remove Availability
      machineToBeRented.isAvailable = false; 
      
     //Return Success 
      return true;
    }

    // Retrieving the machine data necessary for user
    function getRentalMachineInfo(uint machineId) public view returns (string, string, address, address, bool, uint, uint) {
      
      uint totalMachines = getMachineCount();
      require(machineId >= 0 && machineId < totalMachines);
      
      //Get specified machine 
      MachineLib.Machine memory specificMachine = rentals[machineId];
      
      //Return data considered in rental process
      return (specificMachine.make,specificMachine.licenseNumber, specificMachine.owner ,specificMachine.rentee, specificMachine.isAvailable, specificMachine.year , specificMachine.machineId);
    }

    //Add RentableMachine
    function addNewMachine(string make, address owner, string licenseNumber, uint year) public returns (uint) {
        assert(!stopped); 
        //Create machine object within function
        
        //Current # of machines
        uint count = getMachineCount();
        //Increment Count
        //Construct Machine Object
        MachineLib.Machine memory newMachine = MachineLib.Machine(make,true, 0x0 , owner, year,licenseNumber,count);
        
        //Add to Array
        rentals.push(newMachine);
        
         return count;
    }
    
    //Allow Machine Owner to Mark machine as returned
    function returnMachine(uint machineId) public  returns (bool) {
        assert(!stopped); 
        
        //Get Specific machine
        MachineLib.Machine storage specificMachine = rentals[machineId];
        require(specificMachine.owner == msg.sender);
        //Make machine available again
        specificMachine.isAvailable = true;
        //Remove previous rentee
        specificMachine.rentee = 0x0;
        
        //Return Success
        return true;
    }

}
