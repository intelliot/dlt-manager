from http.server import BaseHTTPRequestHandler, HTTPServer
import time
from sys import argv
import random
import json
import web3
import argparse
from web3 import Web3
from web3.gas_strategies.time_based import (fast_gas_price_strategy,
					    medium_gas_price_strategy,
					    slow_gas_price_strategy,
					    glacial_gas_price_strategy)


parser = argparse.ArgumentParser(description="Poll coordinates from AVL")
parser.add_argument("--host_ip",  type=str, help="Assign the IP of the HTML ", default="localhost")
parser.add_argument("--dlt_mgr_ip",  type=str, help="Assigns the IP of the DLT manager", default="0.0.0.0")
args = parser.parse_args()

hostName=args.host_ip
manager_ip=args.dlt_mgr_ip

serverPort = 9090
#manager_ip="0.0.0.0"

print("=============================================================================")

w3 = Web3(Web3.HTTPProvider("http://"+manager_ip+":8545"))
if w3.isConnected():
    print("Successfully connected to Testnet")
else:
    print("Failed to connect to Testnet, please check your network")

led = True

provider_rpc = {
    "development": "http://"+manager_ip+":8545",
    "alphanet": "https://rpc.testnet.moonbeam.network",
}
web3 = Web3(Web3.HTTPProvider(provider_rpc["development"]))  # Change to correct network

# Variables
account_from = {
    "private_key": "0x7968992c32f33408f0abc9ab42aa95d77fb52524c2e0c62d1adef42840d3d502",
    "address": "0xC4D62b4DC0749127b63FB070966c25Ac381c1944",
}
address_to = "0xC4D62b4DC0749127b63FB070966c25Ac381c1944"  # Change address_to

#
#  -- Create and Deploy Transaction --
#
print(
    f'Attempting to send transaction from { account_from["address"] } to { address_to }'
)

# Sign Tx with PK
tx_create = web3.eth.account.signTransaction(
    {
        "nonce": web3.eth.getTransactionCount(account_from["address"]),
        "gasPrice": 1,
        "gas": 21000,
        "to": address_to,
        "value": web3.toWei("1", "ether"),
    },
    account_from["private_key"],
)

# Send Tx and Wait for Receipt
tx_hash = web3.eth.sendRawTransaction(tx_create.rawTransaction)
receipt = web3.eth.waitForTransactionReceipt(tx_hash)

print("Test transaction to Ethereum successful")


class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        a=retrieve_tx(int(post_data.decode('utf-8')))
        self._set_response()
        self.wfile.write(str(a).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        a=send_tx(post_data.decode('utf-8'))
        self._set_response()
        self.wfile.write(str(a).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=serverPort):
    server_address = (hostName,port)
    httpd = server_class(server_address, handler_class)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()


def encode(x):
    return str(x).encode()
def decode(x):
    return x.decode()

def retrieve_tx(blocknr):
    tranid = w3.eth.get_transaction_by_block(blocknr,0)
    savedblock = contract.decode_function_input(tranid.input)[1]
    return(decode(savedblock["time"]))



def send_tx(value):
    blknr=web3.eth.block_number
    timestamp_encode = encode(value)
    nonce = w3.eth.getTransactionCount(account_address)
    function = contract.functions.add_records(timestamp_encode)
    tx = function.buildTransaction({"nonce": nonce,"gasPrice": gas_price})

    try:
        #sign  transaction
        signed_tx = w3.eth.account.signTransaction(tx, private_key)

        #Send tranaction
        tx_hash = w3.eth.sendRawTransaction(signed_tx.rawTransaction)

        #get receive
        receipt = w3.eth.waitForTransactionReceipt(tx_hash)

        #print("Successfully sent to Ethereum")
        #print("gas used:", receipt.cumulativeGasUsed)
        #print("contract address:", contract_address)
        print("tx:", Web3.toHex(receipt.transactionHash))
        print("block:", Web3.toHex(receipt.blockHash))
    except Exception as e:
        print(e)
    print("=============================================================================")
    return(blknr+1)


print("=============================================================================")


print("Setting gas price strategy. Please wait...")
w3.eth.setGasPriceStrategy(fast_gas_price_strategy)
gas_price = w3.eth.generateGasPrice()
print("Estimated gas price {} gwei".format(Web3.fromWei(gas_price, "gwei")))

contract_address = "0xDca4261fB51136E65700Ef63fb7f18943A658094"

abi = json.loads('[ { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "_to", "type": "address" }, { "indexed": false, "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "led", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": false, "internalType": "bytes", "name": "time", "type": "bytes" }], "name": "records", "type": "event" }, { "constant": false, "inputs": [], "name": "destory", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "bytes", "name": "time", "type": "bytes" }], "name": "add_records", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "address", "name": "_to", "type": "address" }, { "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "control_led", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" } ]')

account_address = Web3.toChecksumAddress(0xC4D62b4DC0749127b63FB070966c25Ac381c1944)
private_key = "0x7968992c32f33408f0abc9ab42aa95d77fb52524c2e0c62d1adef42840d3d502"
print("=============================================================================")
contract = w3.eth.contract(address=contract_address, abi=abi)



if len(argv) == 2:
    run(port=int(argv[1]))
else:
    run()




