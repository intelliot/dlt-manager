// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.4.22 <0.9.0;

contract Storage {

    string recorded_data;


    function store(string memory strdata) public {
        recorded_data = strdata;
    }

    /**
     * @dev Return value 
     * @return value of 'data'
     */
    function retrieve() public view returns (string memory){
        return recorded_data;
    }
}