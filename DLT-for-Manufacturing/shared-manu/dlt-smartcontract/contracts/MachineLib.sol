pragma solidity ^0.4.17;

library MachineLib {
    //This library contains helps create machine Objects for Rental Contracts
    struct Machine {
        string make; // machine Model
        bool isAvailable;  // if true, this machine can be rented out
        address rentee; // person delegated to
        address owner; //Owner of machine
        uint year;   // index of the voted proposal
        string licenseNumber; // machine identification
        uint machineId; // index of machine to be rented 
    }
}