var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8042')); // your web3 provider

var transaction = "0x410809802f2f35ba49d1e9ff3741790d908b6fc30f945b5c896b756690c4a549";

web3.eth.getTransaction(transaction, function(err, tx){
    let tx_data = tx.input;
    let input_data = '0x' + tx_data.slice(10);  // get only data without function selector

    let params = web3.eth.abi.decodeParameters(['bytes32', 'string', 'string', 'string'], input_data);
    console.log(params);
});