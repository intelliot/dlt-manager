/*
Script to send values of IoT sensors to Ethereum Blockchain
in form of a transaction. This creates a new block with the
measured values of the IoT sensors encoded as a hex value
for everyone to see.
In this process, realistic random values are initially
generated, which are to simulate the measured data of
the IoT sensors first. These are then packed into a JSON
string, which is encoded in hex format and then sent to
the blockchain in the form of a transaction.
*/

/*calling the all function after a set time in milliseconds*/
setInterval(all,10000);

/*function for all*/
function all () {

/*Various values such as the IP address and the addresses of the sender and $
be entered here to configure the sensor.*/
var from_adr = "\"" + "0xf90c16e1628661bc15769b76c8266343ec35db482239904b0ca1bd90c8fbca01" + "\"";
var to_adr = "\"" + "0x2E6d29B17a526334843141F8BEf0f0424760199e" + "\"";
var ip_adr_port_blockchain = "http://127.0.0.1:8545";

	/*function to determine a value between realistic uppernod
	and lower limit value*/
	var CO2value = Math.round(Math.random() * (2000 - 200) + 200);
	var tempvalue = Math.round(Math.random() * (50 - 10) + 10);
	var airhumidityvalue = Math.round(Math.random() * (95 - 20) + 20);
	var NOxvalue = Math.round(Math.random() * (250 - 40) + 40);
	var SO2value = Math.round(Math.random() * (400 - 40) + 40);
	var oxygenvalue = Math.round(Math.random() * (25 - 10) + 10);


/*setting random vaues in JSON format with associated classification*/
var jsontext = JSON.stringify({
Carbon_Dioxide: CO2value, Temperatur: tempvalue, Air_Humidity: airhumidityvalue,
Nitrogen_Dioxide: NOxvalue, Sulfur_Dioxide: SO2value, Oxygen: oxygenvalue});



/*function for encoding in hex format by stepping through the chars of the string*/
String.prototype.hexEncode = function(){
var hex, i;

var result = "";
for(i=0; i<this.length; i++){
	hex = this.charCodeAt(i).toString(16);
	result += ("000"+hex).slice(-4);
	}
return result;
}

var jsonhex = jsontext.hexEncode();



/*creating a child process and execute the send transaction command in it*/
const { exec } = require('child_process');

exec('curl -X POST -H "Content-Type: application/json" --data \'{"jsonrpc":"2.0", "method":"eth_sendTransaction", \
	 "params": \
		 [ \
			{ \
				"from": ' + from_adr + ',   \
				"to": '+ to_adr + ', 	    \
				"gas": "0x76c0",    	    \
				"gasPrice": "0x4A817C800",  \
				"value": "0x9184e72a", 		\
				"data": "0x' + jsonhex +'"}],\
				"id":1}\' '
				+ ip_adr_port_blockchain + '', (error, stdout, stderr) => {
  if (error) {
    console.error(`error: ${error.message}`);
    return;
  }

  if (stderr) {
    console.error(`stderr: ${stderr}`);
    return;
  }

  console.log(`stdout:\n${stdout}`);
});

}
