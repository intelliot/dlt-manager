On the application Distributed Ledger Technologies in Manufacturing

# Features 
On the scope of this phase, we provide features on the application of DLT in Manufacturing use case

* Design DLT manager and DLT clients for the integration with Robots/Machines
* Interact with UR5 robots based on RTDE interfaces, collect the output from robot controller
* Design smart contract-based terminology to process the data from UR5 Robot Controller
* Record the selected results from robot controller and publish to the DLT 


# Setup

** Blockchain Network 
**Simulation**

For testing your integration, we recommend you try with a emulate Blockchain running locally in directory *sim-dlt-net/*  which provides a simulate blockchain platfoms without contraints. You can test the integration of your part to DLT by publishing your data to DLT via smart contracts.

**Requirements**:

* Docker
* Docker-compose
* NodeJs
* Python3

**Step-by-step**in this sim-dlt-net directory, we have three folders, ganache, truffle, client-app.

* Ganache: a simulation Ethereum platform running locally and open in port 8545.
* Truffle: A tool help to deploy smart contracts to DLT
* Client app: DLT clients to communicate with DLT via Smart Contracts.

**Get Started **Start Blockchain network

```
cd sim-dlt-net

docker-compose up --build
```

if you see the DLT network start with the list of 10 accounts, public/private keys with their balances, it's done. I have configured 10 ETH accounts with fixed private keys and 1000000 ETH for testing, it looks like that:

```
ganache          | Available Accounts
ganache          | ==================
ganache          | (0) 0x2E6d29B17a526334843141F8BEf0f0424760199e (~100000000 ETH)
ganache          | (1) 0xC4D62b4DC0749127b63FB070966c25Ac381c1944 (~100000000 ETH)
ganache          | (2) 0xDF1Db5C73747dB8deD48A3a25C17Af71FBdF1E89 (~100000000 ETH)
ganache          | (3) 0x4988518828cA3aE1E2a3E4dbcce623A40B249102 (~100000000 ETH)
ganache          | (4) 0x936cc17ac0766aE0b5f02Fa1F6BacAE30591313b (~100000000 ETH)
ganache          | (5) 0x5a6571a19e0e444411afb29E8566ba93417C7Ee3 (~100000000 ETH)
ganache          | (6) 0xaA445Bbe0970D210615A37109C734dffBc15f867 (~100000000 ETH)
ganache          | (7) 0xEFAA828E2d2Ca6317fE56a31E44C72390F854006 (~100000000 ETH)
ganache          | (8) 0x53866B55bDc66Ce7a30c20C39F105290C0559aCF (~100000000 ETH)
ganache          | (9) 0x2e94d4AA531df3943Bc09e3eEAa80358a02ad663 (~100000000 ETH)
ganache          |
ganache          | Private Keys
ganache          | ==================
ganache          | (0) 0xf90c16e1628661bc15769b76c8266343ec35db482239904b0ca1bd90c8fbca01
ganache          | (1) 0x7968992c32f33408f0abc9ab42aa95d77fb52524c2e0c62d1adef42840d3d502
ganache          | (2) 0xb492c13c93edc695916522966262ad4dc1c7f64a22b138504c5b250a1c484203
ganache          | (3) 0xb22df97c876895cec01b8ad70b00482e2201dbecc61482a9c1e44544436b1b04
ganache          | (4) 0x69ce909787a29af568092a6d1cb693a3b4510c6d4eebd7d01762aa5f4d824105
ganache          | (5) 0x867f938edd9a5598b40bcf2c68a3106c0fe7486c1a6d50ba9a5171dc865b5306
ganache          | (6) 0xe1921a270bd9751ccc2d04896c7f62cbb5d9cd5dc3195de3fc2d29c483f38507
ganache          | (7) 0x03e88e467120938d05ca0a2677a9a25080e1a16093655bdd3c8fd8c43bd4ba08
ganache          | (8) 0x3b270a195f592a505ce94186793ababb27a33a65a572bc9e7b06766fb6626b09
ganache          | (9) 0x6418670e00dbdb0a6e9bd99f1521f7294f58af0995c7a0534c2021fcd486e710
ganache          |
ganache          | Gas Price
ganache          | ==================
ganache          | 20000000000
ganache          |
ganache          | Gas Limit
ganache          | ==================
ganache          | 6721975
ganache          |
ganache          | Call Gas Limit
ganache          | ==================
ganache          | 9007199254740991
ganache          |
ganache          | Listening on 0.0.0.0:8545

```

and the smart contract is deployed. In this example, a simple smart contract to record strings to DLT is implement. It looks like:

```
ruffle_suite    |    > contract address:    0xDca4261fB51136E65700Ef63fb7f18943A658094
truffle_suite    |    > block number:        7
truffle_suite    |    > block timestamp:     1633947517
truffle_suite    |    > account:             0x2E6d29B17a526334843141F8BEf0f0424760199e
truffle_suite    |    > balance:             99999999.975605839999999999
truffle_suite    |    > gas used:            361904 (0x585b0)
truffle_suite    |    > gas price:           20 gwei
truffle_suite    |    > value sent:          0 ETH
truffle_suite    |    > total cost:          0.00723808 ETH

```

** Smart Contracts
 The detail of smart contract is presented in the RecordData.sol, based on the specific requirement about the output from robot controller, we can add to the contract. 

```
contract RecordData {
    event records(address indexed _from, bytes time, bytes activity);

    function add_records(
        bytes memory time, 
        bytes memory activity
    ) public {
        emit records(msg.sender, time, activity);
    }
}
```

# Operations

