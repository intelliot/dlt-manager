#!/usr/bin/env python


import matplotlib.pyplot as plt

import sys
sys.path.append('/home/user/intelliot/distributed-ledger-technology/DLT-for-Manufacturing/shared-manu/ur5-robot/')
import rtde.csv_reader as csv_reader

with open('robot_data.csv') as csvfile:
    r = csv_reader.CSVReader(csvfile)

# plot target_TCP_speed_1
#plt.plot(r.timestamp, r.target_q_1)
#plt.plot(r.timestamp, r.actual_execution_time)
#plt.plot(r.timestamp, r.joint_temperatures_2)
plt.plot(r.timestamp, r.target_q_1, label='target')
plt.plot(r.timestamp, r.actual_q_1, label='actual')
#plt.plot(r.timestamp, r.target_q_1)
#plt.plot(r.timestamp, r.target_q_2)
#plt.plot(r.timestamp, r.actual_TCP_speed_0)
#plt.plot(r.timestamp, r.actual_TCP_force_0)
#plt.plot(r.timestamp, r.actual_execution_time)
#plt.plot(r.timestamp, r.elbow_velocity)


plt.xlabel('Time [s]')
plt.legend()

plt.show()

