#!/bin/bash

geth --identity "dlt-manager1" \
    --networkid 42 \
    --datadir "/home/user/intelliot/dlt-benchmark/eth-testnet/dlt-manager1/data" \
    --nodiscover \
    --mine \
    --rpc.allow-unprotected-txs \
    --http.port "8545" \
    --port "30303" \
    --unlock 0 \
    --password password.sec \
    #--ipcpath "/home/user/Library/Ethereum/geth.ipc"  \
    --ws.api "admin,eth,net,web3,miner" \
    --syncmode "full" \
    --snapshot=false