import time
from sys import argv
import random
import json
import web3
import argparse
from web3 import Web3
import matplotlib.pyplot as plt
from web3.gas_strategies.time_based import (fast_gas_price_strategy,
					    medium_gas_price_strategy,
					    slow_gas_price_strategy,
					    glacial_gas_price_strategy)


def decode(x):
    return x.decode()

def retrieve_tx(blocknr):
    tranid = w3.eth.get_transaction_by_block(blocknr,0)
    savedblock = contract.decode_function_input(tranid.input)[1]
    return(decode(savedblock["time"]))

def run():
    thelist = []
    for  i in  range(1,until):
        try:
            a = retrieve_tx(i)
            #print(str(a))
            thelist.append(str(a))
            #print(i,a[i])
        except:
            print(i,"readblock")
    return(thelist)

parser = argparse.ArgumentParser(description="Audits manufacturing data")
parser.add_argument("--dlt_mgr_ip",  type=str, help="Assigns the IP of the DLT manager", default="0.0.0.0")
args = parser.parse_args()

manager_ip=args.dlt_mgr_ip

print("=============================================================================")

w3 = Web3(Web3.HTTPProvider("http://"+manager_ip+":8545"))
if w3.isConnected():
    print("Successfully connected to Testnet")
else:
    print("Failed to connect to Testnet, please check your network")

provider_rpc = {
    "development": "http://"+manager_ip+":8545",
    "alphanet": "https://rpc.testnet.moonbeam.network",
}
web3 = Web3(Web3.HTTPProvider(provider_rpc["development"]))  # Change to correct network


print("=============================================================================")

try:
    print("Setting gas price strategy. Please wait...")
    w3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gas_price = w3.eth.generateGasPrice()
    print("Estimated gas price {} gwei".format(Web3.fromWei(gas_price, "gwei")))
except:
    print("blockchain is empty, please initiate operations")

contract_address = "0xDca4261fB51136E65700Ef63fb7f18943A658094"

abi = json.loads('[ { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "_to", "type": "address" }, { "indexed": false, "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "led", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "_from", "type": "address" }, { "indexed": false, "internalType": "bytes", "name": "time", "type": "bytes" }], "name": "records", "type": "event" }, { "constant": false, "inputs": [], "name": "destory", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "bytes", "name": "time", "type": "bytes" }], "name": "add_records", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "internalType": "address", "name": "_to", "type": "address" }, { "internalType": "uint8", "name": "color", "type": "uint8" } ], "name": "control_led", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" } ]')

account_address = Web3.toChecksumAddress(0xC4D62b4DC0749127b63FB070966c25Ac381c1944)
private_key = "0x7968992c32f33408f0abc9ab42aa95d77fb52524c2e0c62d1adef42840d3d502"
print("=============================================================================")
contract = w3.eth.contract(address=contract_address, abi=abi)


until=web3.eth.block_number
print("Chain lenght is", until, "blocks.")


thelist = run()

print("=============================================================================")
robot = []
robottime=[]
hil = []
hiltime = []
laser = []
lasertime = []
mill = []
milltime = []
for item in thelist:
    #print(item)
    try:
        dictitem = json.loads(item)
        if dictitem["device"] == "robot":
            if(not dictitem["data"]):
                robot.append(-1)
                robottime.append(int(dictitem["t"]))
            elif dictitem["data"]["status"]=="no tcp connection to robot":
                robot.append(0)
                robottime.append(int(dictitem["t"]))
            else:
                robot.append(1)
                robottime.append(int(dictitem["t"]))

        elif dictitem["device"] == "laser":
            if(not dictitem["data"]):
                laser.append(-1)
                lasertime.append(int(dictitem["t"]))
            elif dictitem["data"]["state"]=="available":
                laser.append(0)
                lasertime.append(int(dictitem["t"]))
            else:
                laser.append(1)
                lasertime.append(int(dictitem["t"]))
        elif dictitem["device"] == "hil":
            if(not dictitem["data"]):
                hil.append(-1)
                hiltime.append(int(dictitem["t"]))
            elif dictitem["data"]["state"]=="available":
                hil.append(0)
                hiltime.append(int(dictitem["t"]))
            else:
                hil.append(1)
                hiltime.append(int(dictitem["t"]))
        elif dictitem["device"] == "mill":
            if(not dictitem["data"]):
                mill.append(-1)
                milltime.append(int(dictitem["t"]))
            elif dictitem["data"]["state"]=="available":
                mill.append(0)
                milltime.append(int(dictitem["t"]))
            else:
                mill.append(1)
                milltime.append(int(dictitem["t"]))
        else:
            print("unexpected  device")
    except:
        print("inapropriate block \n", item)


print("TIME IS IN SECONDS")
print("STATUS = -1 offline; 0 available; 1 occupied")

if len(robot) != 0:
    times = [value - robottime[0] for value in robottime]
    print("robot:",robot,len(robot))
    print(times)
    plt.plot(times,robot)
else:
    print("Robot OUT OF REACH")

if len(laser) != 0:
    times = [value - lasertime[0] for value in lasertime]
    print("laser:",laser,len(laser))
    print(times)
    plt.plot(times,laser)
else:
    print("Laser OUT OF REACH")


if len(hil) != 0:
    times = [value - hiltime[0] for value in hiltime]
    print("hil:",hil,len(hil))
    print(times)
    plt.plot(times,hil)
else:
    print("HIL OUT OF REACH")

if len(mill) != 0:
    times = [value - milltime[0] for value in milltime]
    print("mill:",mill,len(mill))
    print(times)
    plt.plot(times,mill)
else:
    print("MILL OUT OF REACH")


