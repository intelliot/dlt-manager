#!/bin/bash

geth --identity "dlt-manager2" \
    --networkid 42 \
    --datadir "/home/user/intelliot/dlt-benchmark/eth-testnet/dlt-manager2/data" \
    --nodiscover \
    --mine \
    --rpc.allow-unprotected-txs \
    --http.port "8545" \
    --port "30304" \
    --unlock 0 \
    --password password.sec \
   # --ipcpath "/home/user/intelliot/dlt-benchmark/eth-testnet/dlt-manager2/data/geth.ipc"  \
    --ws.api "admin,eth,net,web3,miner" \
    --syncmode "full" \
    --snapshot=false